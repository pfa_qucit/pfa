from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from datetime import datetime

from app.views.metadb import metadata, Zone, Area, Error

engine = create_engine('postgresql://root:root@localhost/pfa')
session = Session(engine)
zones = session.query(Zone).all()
for z in zones:
    print z.name
    for a in z.areas:
        print a.file_name
        for e in a.errors:
            print "lat : [%s, %s]" % (e.lat_min, e.lat_max)
            print "lng : [%s, %s]" % (e.lng_min, e.lng_max)
    print "--"

# Ajout d'un erreur:
# ###############################
# new_area = Area(
#     file_name="fakeimg.tiff",
#     timestamp=datetime.now(),
#     AI_version = 1,
#     zone = zones[0]
# )
# session.add(new_area)
#
# new_error = Error(
#     lat_min=42.12,
#     lat_max=42.42,
#     lng_min=33.12,
#     lng_max=33.42,
#     area=new_area,
#     polygon_id=12
# )
# session.add(new_error)
# session.commit()

session.close()
