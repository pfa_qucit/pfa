#Provides function convert_osm_to_csv(min_lon, min_lat, max_lon, max_lat, img_name, output_path)

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../app'))
import OsmApi as osmapi
import config.development as config


api = osmapi.OsmApi(config={'API_URL': "https://www.openstreetmap.org"})

def convert_osm_to_csv(min_lon, min_lat, max_lon, max_lat, img_name, output_path):
    """
    Print in the file 'output_path' the csv file including all buildings in the given square
    """
    f = open(output_path, "w")
    buf = print_header()#buf contains the out string
    data = api.Map(min_lon, min_lat, max_lon, max_lat)#data stores all datas in the given square
    #Buildings are stored either as Way either as Relation of multiple Ways (inner Way, outer Way)
    ways = get_ways(data)#Single Building
    for w in ways:
        if(way_is_building(w)):
            nodes_id = get_nodes(w)
            if(len(nodes_id) > 0):
                buf += print_polygon_outer(nodes_id, w["id"], img_name, data)

    relations = get_relations(data)

    for r in relations:
        if(relation_is_building(r)):
            buf += print_relation(r, img_name, data)
    f.write(buf)
    f.close()

def print_header():
    return "ImageId,BuildingId,PolygonWKT,Confidence\n"

def get_ways(data):
    ways = []

    for d in data:
        if(d["type"] == 'way'):
            ways.append(d["data"])

    return ways

def get_relations(data):
    relations = []

    for d in data:
        if(d["type"] == 'relation'):
            relations.append(d["data"])

    return relations

def way_is_building(way):
    return way["tag"].has_key('building')

def relation_is_building(relation):
    return relation["tag"].has_key('building')

def get_nodes(way):
    nodes_id = []
    for m in way["nd"]:
        nodes_id.append(m)

    return nodes_id

def get_node(id, data):
    for e in data:
        if(e["type"] == "node"):
            if(e["data"]["id"] == id):
                return e["data"]

    return api.NodeGet(id)

def get_way(id, data):
    for e in data:
        if(e["type"] == "way"):
            if(e["data"]["id"] == id):
                return e["data"]

    return api.WayGet(id)

def get_ways_from_relation(relation, data):
    ways_id = []
    ways = []

    for e in relation["member"]:
        if(e["type"] == "way"):
            ways_id.append(e["ref"])

    for w in ways_id:
        ways.append(get_way(w, data))

    return ways

def print_nodes(nodes_id, data):
    res = ""
    first = True

    res += "("
    for n in nodes_id:
        current_node = get_node(n, data)

        if(not first):
            res += ", "

        res += str(current_node["lat"]) + " " + str(current_node["lon"]) + " 0"
        first = False

    res += ")"
    return res

def print_polygon_outer(nodes_id, id_building, img_name, data):
    res = img_name + ","
    res += str(id_building) + ",\"POLYGON ("
    res += print_nodes(nodes_id, data)

    res += ")\", 1.000000\n"
    return res

def print_relation(relation, img_name, data):
    first = True
    res = img_name + "," + str(relation["id"]) + ","
    res += "\"MULTIPOLYGON ("

    for m in relation["member"]:#print outer
        if(m["type"] == "way" and m["role"] == "outer"):
            if(not first):
                res += ","
            way = get_way(m["ref"], data)
            nodes_id = get_nodes(way)
            if(len(nodes_id) > 0):
                res += print_nodes(nodes_id, data)
                first = False

    for m in relation["member"]:#print inner
        if(m["type"] == "way" and m["role"] == "inner"):
            if(not first):
                res += ","
            way = get_way(m["ref"], data)
            nodes_id = get_nodes(way)
            if(len(nodes_id) > 0):
                res += print_nodes(nodes_id, data)
                first = False


    res += ")\", 1.000000\n"

    return res
