import parser as psr
import iou

import csv
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../app/views'))
from metadb import Error
from metadb import ErrorType

#Shapely-1.6.3
from shapely.geometry import Polygon
from shapely.geometry import MultiPolygon
import shapely as sh
import matplotlib.pyplot as plt




def plot_polygon(p,color="k"):
    """
    Takes polygon
    print a polygon with matplotlib
    """
    l = p.exterior.coords
    n = len(l)
    for k in range(n):
        plt.plot([l[k][1],l[(k+1)%n][1]],[l[k][0],l[(k+1)%n][0]],color)

def plot_bounds(e,color="g"):
    """
    Takes error
    print bounds of error e with matplotlib
    """
    plt.plot((e.lng_min,e.lng_min),(e.lat_min,e.lat_max),color)
    plt.plot((e.lng_max,e.lng_max),(e.lat_min,e.lat_max),color)
    plt.plot((e.lng_min,e.lng_max),(e.lat_min,e.lat_min),color)
    plt.plot((e.lng_min,e.lng_max),(e.lat_max,e.lat_max),color)




def compare(osm_polygons, predicted_polygons, threshold_cmp=0, threshold_iou=0.7):
    """
    Takes lists of polygons
    Returns a list of Error objects
    """
    error = []
    #for osm_polygons
    marked_indexes = [0 for k in range(len(osm_polygons))]
    for p in predicted_polygons:
        cmp = []
        cmp_ind = []
        c = 0
        for q in osm_polygons:#search comparables polygons
            if p[2].intersection(q[2]).area > threshold_cmp:
                cmp_ind.append(int(q[1]))
                cmp.append(q[2])
                marked_indexes[c] = 1
            c += 1
        n = len(cmp)
        if n == 0: #No comparable polygon found
            b = p[2].bounds
            e = Error(lat_min = b[0],
                      lat_max = b[2],
                      lng_min = b[1],
                      lng_max = b[3],
                      polygon_id = p[1],
                      item_type = p[3],
                      error_type = ErrorType.NO_MATCH.value,
                      iou_level = 0)
            error.append(e)
        elif n == 1: #One comparable polygon found
            i = iou.iou(p[2],cmp[0])
            if i < threshold_iou:
                b = p[2].bounds
                e = Error(lat_min = b[0],
                          lat_max = b[2],
                          lng_min = b[1],
                          lng_max = b[3],
                          polygon_id = p[1],
                          item_type = p[3],
                          error_type = ErrorType.LOW_IOU.value,
                          iou_level = i)
                error.append(e)
        else: #More than one comparables polygons found
            i = iou.iou(p[2],MultiPolygon(cmp))#
            if i < threshold_iou:
                b = p[2].bounds
                e = Error(lat_min = b[0],
                          lat_max = b[2],
                          lng_min = b[1],
                          lng_max = b[3],
                          polygon_id = None,
                          item_type = 'multipolygon',
                          error_type = ErrorType.LOW_IOU.value,
                          iou_level = i)
                error.append(e)
    #search polygons in osm_polygons that have not been marked
    for k in range(len(marked_indexes)):
        if marked_indexes[k] == 0:
            b = osm_polygons[k][2].bounds
            e = Error(lat_min = b[0],
                      lat_max = b[2],
                      lng_min = b[1],
                      lng_max = b[3],
                      polygon_id = -1,
                      item_type = None,
                      error_type = ErrorType.NO_PREDICTION.value,
                      iou_level = 0)
            error.append(e)
    return error




if __name__ == '__main__':
    path = psr.files_path()
    osm_polygons = psr.parse_csv(path[0])
    predicted_polygons = psr.parse_csv(path[1])
    for p in predicted_polygons:
        plot_polygon(p[2],'b')
    for p in osm_polygons:
        plot_polygon(p[2],'k')

    error = compare( osm_polygons, predicted_polygons, 0, 0.7)
    print(str(len(error))+" errors detected")
    for e in error:
        plot_bounds(e,'g')
    plt.show()
