#Shapely-1.6.3
from shapely.geometry import Polygon
from shapely.geometry import MultiPolygon




def iou_polygon(polygon_a, polygon_b):
    """
    Takes two polygons
    Returns iou level
    """
    i = polygon_a.intersection(polygon_b).area
    if (polygon_a.area + polygon_b.area - i != 0):
        return  i/(polygon_a.area + polygon_b.area - i)
    else:
        return 0.0


def iou_polygon_multipolygon(polygon_a, multipolygon_b):
    """
    Takes polygon and multipolygon
    Returns iou level
    """
    i = 0
    u = polygon_a.area + multipolygon_b.area
    for k in range(len(multipolygon_b)):
        inter = multipolygon_b.geoms[k].intersection(polygon_a).area
        i += inter
    u -= i
    return i / u


def iou_multipolygon(multipolygon_a, multipolygon_b):
    """
    Takes multipolygons
    Returns iou level
    """
    i = 0
    u = multipolygon_a.area + multipolygon_b.area
    for k in range(len(multipolygon_a)):
        for j in range(len(multipolygon_b)):
            inter = multipolygon_a.geoms[k].intersection(multipolygon_b.geoms[j]).area
            i += inter
    u -= i
    return i / u


def iou(polygon_a, polygon_b):
    """
    Takes multipolygons or polygons
    Returns iou level
    """
    if isinstance(polygon_a,Polygon) and isinstance(polygon_b,Polygon):
        return iou_polygon(polygon_a, polygon_b)
    elif isinstance(polygon_a,Polygon) and isinstance(polygon_b,MultiPolygon):
        return iou_polygon_multipolygon(polygon_a, polygon_b)
    elif isinstance(polygon_a,MultiPolygon) and isinstance(polygon_b,Polygon):
        return iou_polygon(polygon_b, polygon_a)
    elif isinstance(polygon_a,MultiPolygon) and isinstance(polygon_b,MultiPolygon):
        return iou_multipolygon(polygon_a, polygon_b)
    else:
        raise("arguments should be polygons and/or multipolygons")
        return  0
