import osm_to_csv as osmcsv

def test():#Example
    min_lon = 2.333333#Paris coords
    min_lat = 48.866667
    dlon = 0.003
    dlat = 0.003
    osmcsv.convert_osm_to_csv(min_lon, min_lat, min_lon + dlon, min_lat + dlat, "test_tiff", "test.csv")
    print("Test ok")
    #test_tiff corresponds to the tiff giving the coordinates (tiff_treatment file allows to get coords from a tiff)

test()
