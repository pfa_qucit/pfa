#This file provides read_naip, pixel_to_lat_lon, and size_dataset functions (various informations about tiff file).

import sys, csv, re

from shapely.geometry import Polygon
from shapely.geometry import MultiPolygon
from osgeo import gdal

def read_naip(file_path):#Returns a dataset from a tiff file
    raster_dataset = gdal.Open(file_path, gdal.GA_ReadOnly)

    if(not raster_dataset):
        print("Error loading file \"" + file_path + "\"")

    return raster_dataset

def pixel_to_lat_lon(raster_dataset, col, row):#Returns the latitude and longitude of a given pixel
    ds = raster_dataset
    xoff, res_w_e, _, yoff, _, res_n_s = ds.GetGeoTransform()

    longitude = res_w_e * col + xoff
    latitude = res_n_s * row + yoff
    return latitude, longitude

def size_dataset(raster_dataset):#Returns the dataset size, in pixel
    ds = raster_dataset
    return (ds.RasterXSize, ds.RasterYSize)

def test():#Example
    raster_dataset = read_naip("TIFF/MUL_AOI_3_Paris_img3.tif")
    print(pixel_to_lat_lon(raster_dataset, 0, 0))
    (w,h) = size_dataset(raster_dataset)
    print(w, h)
    print(pixel_to_lat_lon(raster_dataset, w, h))

#test()
