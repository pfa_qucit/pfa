import sys
import csv
import re

from shapely.geometry import Polygon
from shapely.geometry import MultiPolygon

# Functions

def files_path():
    argc = len(sys.argv)
    if (argc < 2):
        raise Exception("No arguments")
    elif (argc == 2):
        path = [sys.argv[1]]
        return path
    elif (argc == 3):
        path = [sys.argv[1], sys.argv[2]]
        return path
    else :
        raise Exception("Too many arguments")

# pases a polygon_wkt
# returns a shapely polygon
def parse_polygon_wkt(polygon_wkt):
    coord = re.findall('[0-9]+\.?[0-9]* [0-9]+\.?[0-9]*', polygon_wkt)
    for i in range(len(coord)):
        coord[i] = coord[i].split()
        coord[i][0] = float(coord[i][0])
        coord[i][1] = float(coord[i][1])
    polygon = Polygon(coord)
    return polygon

# parses wkt or a list of wkt
# returns a shapely polygon that can have hole(s)
def parse_wkt(wkt):
    item_type = "unknown"
    if type(wkt) is str:
        if (re.search('MULTIPOLYGON', wkt)):
            item_type = "relation"
            polygons = wkt.split("),(")
            if (len(polygons) == 0):
                raise Exception("Empty list")
            elif (len(polygons) == 1):
                polygon = parse_polygon_wkt(polygons[0])
            else:
                inners = []
                outer = parse_polygon_wkt(polygons[0])
                for i in range(1, len(polygons)):
                    inners.append(parse_polygon_wkt(polygons[i]))
                polygon = Polygon(outer.exterior.coords, [inner.exterior.coords for inner in inners])
        elif (re.search('POLYGON', wkt)):
            item_type = "way"
            polygon = parse_polygon_wkt(wkt)
        else:
            raise Exception("Unknown polygon type")
    else:
        raise Exception("Expected a string")
    return polygon, item_type

# parses a csv file
# returns a list of triplets : [img_id, polygon_id, polygon]
def parse_csv(csv_file):
    """ 1st csv : true polygons, 2nd csv : predicted polygons
     Returns a list of triplets : [img_id, polygon_id, polygon]"""
    polygon_lst = []
    with open(csv_file, 'r') as pol_csv:
        pol_csv.readline()
        reader = csv.reader(pol_csv)
        for row in reader:
            img_id = row[0]
            polygon_id = int(row[1])
            wkt = row[2]
            quadruplet = [img_id, polygon_id, parse_wkt(wkt)[0], parse_wkt(wkt)[1]]
            polygon_lst.append(quadruplet)
    return polygon_lst
