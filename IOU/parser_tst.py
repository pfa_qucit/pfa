import parser as psr

# Tests 3 different cases of polygons
def tst__parse_wkt():
    # Non-empty polygon
    polygon_wkt = "POLYGON ((0 0 0, 0 5 0, 5 5 0, 7 3 0))"
    print polygon_wkt, type(polygon_wkt)
    polygon, p_type = psr.parse_wkt(polygon_wkt)
    print polygon, type(polygon), "\n"

    # Empty polygon
    polygon_wkt_emp = "POLYGON EMPTY"
    print polygon_wkt_emp, type(polygon_wkt_emp)
    polygon_emp, emp_type = psr.parse_wkt(polygon_wkt_emp)
    print polygon_emp, type(polygon_emp), "\n"

    # Polygon with a hole
    polygon_wkt_hole = "MULTIPOLYGON ((1 1 0, 1 2 0, 2 2 0, 2 1 0),(0 0 0, 0 3 0, 3 3 0, 3 0 0))"
    print polygon_wkt_hole, type(polygon_wkt_hole)
    polygon_hole, hole_type = psr.parse_wkt(polygon_wkt_hole)
    print polygon_hole, type(polygon_hole)
    print "outer :", list(polygon_hole.exterior.coords)
    print "inners : ", list(polygon_hole.interiors)
    return

# Tests the parsing of a given CSV, prints the parsed polygons
def tst__parse_csv():
    path = psr.files_path()
    p_lgth = len(path)
    if (p_lgth == 1):
        polygon_lst = psr.parse_csv(path[0])
        for p in polygon_lst:
            if not p[2].is_empty:
                print(p[0], p[1], p[2].exterior.coords, list(p[2].interiors))
            else:
                print(p[0], p[1], p[2])
    else:
        print "one argument please"
    return


if __name__ == '__main__':
    print "==== Test parse_wkt ===="
    tst__parse_wkt()

    print "\n==== Test parse_csv ===="
    tst__parse_csv()
