import sys , os
import parser as psr
import tif_treatment as tif_t
import compare as cmp
sys.path.append(os.path.join(os.path.dirname(__file__), '../app/views'))
import osm_to_csv as osmcsv
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from datetime import datetime

from metadb import metadata, Zone, Area, Error

def gen_csv(csv_name, lng_min, lat_min, lng_max, lat_max, img_name):
    osmcsv.convert_osm_to_csv(lng_min, lat_min, lng_max, lat_max, img_name,csv_name)



def treat_zone(tif_path, csv_path_osm, csv_path_ai, zone, ai_version):
    """
    Takes the tif_path to get coordinates, the path to register csv from osm, the path to find csv file,
    the zone which is a string, the AI_version number
    fill the db with errors find on the tif given
    """
    #instance area
    engine = create_engine('postgresql://root:root@localhost/pfa')
    session = Session(engine)
    zone = session.query(Zone).filter(Zone.name == "Paris").first()#only Paris for the moment
    a = session.query(Area).filter(Area.file_name == os.path.basename(tif_path)).first()
    if a == None:
        a = Area(file_name=tif_path, timestamp=datetime.now(), AI_version=ai_version, zone=zone)

    #load the tif and computing coordinates
    raster_dataset = tif_t.read_naip(tif_path)
    lat_1, lng_1 = tif_t.pixel_to_lat_lon(raster_dataset, 0, 0)
    w, h = tif_t.size_dataset(raster_dataset)
    lat_2, lng_2 = tif_t.pixel_to_lat_lon(raster_dataset, w, h)
    lat_min,lat_max = min(lat_1,lat_2),max(lat_1,lat_2)
    lng_min,lng_max = min(lng_1,lng_2),max(lng_1,lng_2)

    ##get csv file with osm polygons and load it
    gen_csv(csv_path_osm,lng_min , lat_min, lng_max, lat_max, tif_path)
    osm_polygons = psr.parse_csv(csv_path_osm)

    ##load deep learning model's predictions
    predicted_polygons = psr.parse_csv(csv_path_ai)

    ##compare theses csv files
    errors = cmp.compare( osm_polygons, predicted_polygons, threshold_cmp=0, threshold_iou=0.7)
    print(str(len(errors))+" detected errors in "+tif_path)

    ##fill the database

    session.add(a)
    for e in errors:
        #add error
        e.setArea(a)
        session.add(e)

    session.commit()
    session.close()


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("Usage : python scheduler.py <path_to_tif_file>  <path_to_osm_csv> <path_to_AI_csv> <Zone>")

    tif_path = sys.argv[1]
    csv_path_ai = sys.argv[2]
    csv_path_osm = sys.argv[3]
    zone = sys.argv[4]
    treat_zone(tif_path,csv_path_osm,csv_path_ai,zone,1)
