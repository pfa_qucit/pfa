import iou

#Shapely-1.6.3
from shapely.geometry import Polygon
from shapely.geometry import MultiPolygon

A = Polygon([(0,0),(0,2.0),(2,2),(2,0)])
B = Polygon([(1,1),(3,1),(3,3),(1,3)])
C = Polygon([(1,0),(3,0),(3,1),(1,1)])
D = MultiPolygon([ B, C])
E = Polygon([(2,0),(4,0),(4,1),(2,1)])
F = MultiPolygon([ A, E])
G = Polygon([(0,0),(0,3),(3,3),(3,0)])
H = Polygon([(1,0),(1,1),(0,1),(0,3),(3,3),(3,0)])

if __name__ == '__main__':
    print "==== Test iou_polygon ===="
    if iou.iou(A,B) == 1.0/7 and iou.iou(G,H) == 8.0/9:
        print("test passed")
    else:
        print("test failed")

    print "\n==== Test iou_polygon_multipolygon ===="
    if iou.iou(A,D) == 1.0/4:
        print("test passed")
    else:
        print("test failed")
    print "\n==== Test iou_multipolygon ===="
    if iou.iou(D,F) == 1.0/3:
        print("test passed")
    else:
        print("test failed")
