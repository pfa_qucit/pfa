
# Project Name #

Description

## Getting Started ##

### Prerequisites ###

A virtual environment can be built to facilitate the configuration of the environment.
It is usable throught the following command :

```
pip install flask requests requests_oauthlib shapely matplotlib
gdal (https://gist.github.com/cspanring/5680334)
export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal

```
### Install Database ###
First, you must create the database in your server.
Next, please configure the database access the script `init_database.py`:
```
SGBD = 'postgresql'
SGBD_USERNAME = 'root'
SGBD_PASSWORD = 'root'
SGBD_ADDR = 'localhost'
DB_NAME = 'osm_errors'
```
Finaly, to generate the database schema, execute the python script :
```
python init_database.py
```
### Fill Database ###

Run the comparison between OSM polygons and AI's predictions and fill the database with it.
<path_to_tif_file> the path to the tif file of the zone
<path_to_osm_csv> the path where the program will write the csv file of osm's polygons
<path_to_AI_csv> the path to the csv file produced by the AI
<Zone> a string describing the zone, for example "Paris"
csv and tif files can respectively be placed in IOU/CSV and IOU/TIF
```
python IOU/scheduler.py <path_to_tif_file>  <path_to_osm_csv> <path_to_AI_csv> <Zone>
```

Debug:  Run the comparison between two CSV files and plot errors with matplotlib


```
python IOU/compare.py <path_to_OSM_csv> <path_to_AI_csv>
```




### Running ###

Run the website

```
python run.py
```

The current website GUI will be accessible via your favourite browser at the address displayed on your CLI.


## Related Links ##

#### DeepOSM
> https://github.com/trailbehind/DeepOSM

#### GeoFabrik
> http://download.geofabrik.de/

#### Restful API with Flask
> https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask

#### SpaceNet contest
> https://community.topcoder.com/longcontest/?module=ViewProblemStatement&rd=16892&pm=14551

#### SpaceNet on AWS
> https://aws.amazon.com/public-datasets/spacenet/

#### Polygon-related links
> http://www.siggraph.org/education/materials/HyperGraph/scanline/outprims/polygon1.htm
> http://geomalgorithms.com/a03-_inclusion.html#Pseudo-code

#### SpaceNet Utilities
> https://github.com/SpaceNetChallenge/utilities

#### Winner of the second SpaceNet Challenge
> https://github.com/SpaceNetChallenge/BuildingDetectors_Round2/tree/master/1-XD_XD/

#### Atom plugin for python linter - follow the instruction to remove E501 errors
> https://atom.io/packages/linter-pycodestyle
