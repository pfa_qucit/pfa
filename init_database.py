# -*- coding: utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from app.views.metadb import metadata, Zone, Area, Error

SGBD = 'postgresql'
SGBD_USERNAME = 'root'
SGBD_PASSWORD = 'root'
SGBD_ADDR = 'localhost'
DB_NAME = 'pfa'


def init_database():
    """
    Supprime les tables de la BDD si elles existent déjà puis
    Crée toutes les tables du model puis ajoute la zone Paris
    """
    print("Creating DB...")
    engine = create_engine("%s://%s:%s@%s/%s"%(SGBD, SGBD_USERNAME, SGBD_PASSWORD, SGBD_ADDR, DB_NAME))
    metadata.drop_all(engine)
    metadata.create_all(engine)
    session = Session(engine)
    session.add(Zone("Paris"))
    session.commit()
    session.close()
    print("DB Created")

init_database()
