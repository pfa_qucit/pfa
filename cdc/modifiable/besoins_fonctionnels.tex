\chapter{Expression des besoins} \label{chapter:deux}

Afin de définir clairement l'ensemble des besoins liés à la réalisation de ce projet, il est utile de présenter un ensemble de cas d'utilisation complets permettant de visualiser la manière dont le client utilisera l'outil développé. 

Ces scénarios s'accompagnent de la définition d'items présentant le périmètre global de l'outil et permettant d'identifier les différents types de besoins, leur profondeur et leurs interactions.


\section{Identification des acteurs}

L'ensemble des acteurs intégrés dans le fonctionnement de cet outil sont décrits par le diagramme et le paragraphe suivants : 

\begin{figure}[ht]
    \centering
  \makebox[\textwidth]{\includegraphics[width=0.7\paperwidth]{figures/acteurs.png}}
  \caption{Acteurs du système}
  \label{fig:boundary}
\end{figure}

\begin{itemize}
    \item[Mappeur : ] utilisateur de l'outil, qui va comparer les différences entre les tracés OSM et les polygones générés par le modèle.
    \item[Interface web : ]  interface proposée à l'utilisateur pour choisir une zone à vérifier, corriger des erreurs et les envoyer à OSM
    \item[Détection et stockage des erreurs : ] scripts s'exécutant régulièrement pour remplir la base de données de potentielles erreurs. Ces erreurs sont détectées après avoir interagi avec le serveur de calcul et en ayant comparé ses résultats avec les données OSM.
    \item[OpenStreetMap : ] API fournie par OpenStreetMap, qui permet à l'outil développé de proposer une connexion, de récupérer les contours de bâtiments, ainsi que d'envoyer les corrections.
    \item[Serveur de calculs : ] serveur permettant de faire tourner le modèle d'intelligence artificielle.
    \item[Base de données : ] base de données développée pour stocker toutes les informations utiles au fonctionnement de l'outil (stockage des erreurs).
    \item[Images satellites : ] images satellites de différentes villes fournies par le client. Ces images sont stockées en dur.
\end{itemize}

\section{Scénarios d'utilisation} \label{section:scenario}

\subsubsection{(S1) Scénario global}

\begin{enumerate}
    \item Le mappeur se rend sur le site web interfaçant l'outil développé par l'équipe et se connecte à l'aide de son compte OSM.
    \item Le mappeur peut alors naviguer et zoomer sur une carte satellite, restreinte aux villes étudiées.
    \item Les erreurs, calculées en amont, sont affichées sur la carte par des marqueurs ; le mappeur a la possiblité d'afficher les calques des bâtiments OSM.
    \item Le mappeur peut  alors, pour chaque erreur fournie, modifier ou supprimer le polygone déjà présent sur OSM s'il y en a un, ajouter un polygone ou ne rien faire sinon.
    \item Plusieurs choix s'offrent alors au mappeur :
    \begin{itemize}
        \item[-] Le mappeur peut à tout moment, envoyer au serveur OSM les modifications qu'il a effectuées ou celles proposées initialement par le modèle.
        \item[-] Le mappeur peut abandonner les modifications qu'il a effectuées, ce qui annule les changements apportés sur la session et ne notifie pas OSM des changements à appliquer.
    \end{itemize}
    %\item[-] Le mappeur peut à tout moment se connecter à son compte OSM.
    %\item[-] Le mappeur peut à tout moment accéder à diverses statistiques [STATISTIQUES A DEFINIR]
    
    \item Le mappeur se déconnecte de son compte OSM
\end{enumerate}

Ce cas nominal est présenté dans la figure \ref{fig:scenario_global} :

\newpage
\vspace*{-15mm}
\begin{figure}[H]
\centering
\includegraphics[height=\textheight]{figures/scenario_global}
\captionof{figure}{Scénario d'utilisation global}
\label{fig:scenario_global}
\end{figure}

\subsubsection{(S2) Scénario interne}

Tel quel, ce scénario principal décrit l'aspect externe de l'application et son interaction avec l'utilisateur. Il échoue cependant  à démontrer le second aspect de l'outil, qui se déroule en interne sur le serveur. Cet aspect est décrit par le scénario suivant :

\begin{enumerate}
    \item[(0.)] En interne, le serveur sur lequel est présent l'interface a également accès à des modèles donnés par le client. Ces modèles sont des instances de l'intelligence artificielle entraînées sur divers sets %ensembles plutôt ?
    de données ayant pour but de localiser les contours de bâtiments. Un modèle n'est valide que sur la zones pré-définie sur laquelle il a été entraîné.
    \item Des scripts présents sur le serveur et développés dans le cadre du projet permettent d'interagir avec ces instances. \textit{Régulièrement}, des détections d'erreurs sont lancées par ces scripts à l'aide d'images satellite. Ces détections se déroulent de la manière suivante:
    \begin{enumerate}
        \item Les scripts récupèrent des images satellite propres à la zone géographique sur laquelle la détection d'erreur est faite. Ces images sont passées en entrée de l'instance du modèle. 
        \item En interne, le modèle reposant sur un réseau neuronal détermine à partir des entrées ci-dessus, un ensemble de polygones représentant les contours supposés de bâtiments sur la zone demandée. Cette étape est totalement opaque pour l'équipe et est gérée par le code déjà développé par le client.
        \item  Les polygones de la sortie du modèle sont comparés à  ceux des données OSM, afin de déterminer les différences pouvant potentiellement être des erreurs sur OSM.
        \item L'information indiquant la recherche d'erreurs sur la zone à un instant T est retenue, éventuellement associée aux polygones ci-dessus afin de préciser un timestamp utile en cas d'évolution du modèle, de retrait futur ....
    \end{enumerate}
    %\item Selon le seuil d'erreur acceptable défini arbitrairement, les polygones proposés par le modèle sont sauvegardés sous une forme adaptée pour le reste de leur traitement au sein de l'outil (cf. scénario précédent).
\end{enumerate}

Il est à noter que ce second scénario se répète naturellement à intervalles fréquents pour chaque zone.


Après avoir défini ces scénarios principaux, il nous est naturellement possible de déterminer différents cas spéciaux d'utilisation, indiquant les différentes interactions de l'utilisateur, possibles avec l'outil. Les différents sous-scénarios alternatifs identifiés sont :

\newpage
\subsubsection{(S3) Scénario authentification}

\noindent Prérequis : l'utilisateur vient d'arriver sur notre interface web\\
Situation : l'utilisateur souhaite se connecter à notre application

\begin{figure}[H]
\centering
\includegraphics[height=0.7\textwidth]{figures/scenario_authentification}
\captionof{figure}{Scénario d'authentification}
\label{fig:scenario_authentification}
\end{figure}

\newpage
\subsubsection{(S4) Scénario de navigation sur la carte}

\noindent Prérequis : (S3) + les erreurs ont été calculées et stockées dans une base de données\\
Situation : l'utilisateur souhaite naviguer sur la carte afin d'observer des  erreurs

\begin{figure}[H]
\centering
\includegraphics[height=0.8\textwidth]{figures/scenario_navigation}
\captionof{figure}{Scénario navigation}
\label{fig:scenario_navigation}
\end{figure}

\newpage
\subsubsection{(S5) Scénario de correction d'une erreur}

\noindent Prérequis : (S4) + l'utilisateur a suffisamment zoomé pour qu'une erreur s'affiche\\
Situation : l'utilisateur souhaite corriger ou invalider une erreur qui a été signalée

\begin{figure}[H]
\centering
\includegraphics[height=0.7\textwidth]{figures/scenario_correction}
\captionof{figure}{Scénario correction}
\label{fig:scenario_correction}
\end{figure}

\newpage
\subsubsection{(S6) Scénario de soumission des corrections}

\noindent Prérequis : (S4) + l'utilisateur a effectué une ou plusieurs correction(s) dans (S5)\\
Situation : l'utilisateur souhaite soumettre les modifications apportées à OSM

\begin{figure}[H]
\centering
\includegraphics[height=0.7\textwidth]{figures/scenario_soumission}
\captionof{figure}{Scénario soumission}
\label{fig:scenario_soumission}
\end{figure}

\newpage
\section{Besoins fonctionnels}

À partir des cas d'utilisation indiqués ci-dessus, les différentes fonctionnalités attendues pour l'outil se dégagent. Nous répertorions ces besoins fonctionnels ainsi que les diverses questions qu'infère leur développement. Une vue d'ensemble est présentée sur la figure \ref{fig:cas_utilisation}


\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{figures/cas_utilisation}
\captionof{figure}{Diagramme de cas d'utilisation}
\label{fig:cas_utilisation}
\end{figure}

\subsection{Cas nominal (interface web)}


\subsubsection{(B1) Déterminer une zone d'action ; (B2) Afficher des erreurs enregistrées}

L'utilisateur pourra choisir la zone qu'il voudra traiter en modifiant sa vue courante de la carte, il pourra donc effectuer des translations de la carte et zoomer/dézoomer sur celle-ci.

Le mappeur choisit parmi des ensembles de zones, mis à disposition de manière statique par l'outil (Paris dans un premier temps). En interne, l'interface web détermine la zone à afficher (correspondance ville => coordonnées GPS) et présente à l'aide d'une librairie tierce une carte de cette zone. Les erreurs propres à cet espace sont récupérées en base de données (ce qui implique une correspondance erreurs / zones), et affichées (traçage de polygones correspondant aux polygones OSM). L'utilisateur navigue librement sur la carte affichée pour voir et corriger les erreurs. 

\subsubsection{(B3) Corriger une zone}


Les erreurs détectées seront affichées sur la carte à l'aide de marqueurs. Par souci de lisibilité, les marqueurs proches pourront être regroupés lorsque le niveau de zoom ne sera pas assez suffisant. Dans le même esprit, les polygones représentant les bâtiments ne seront affichés qu'à partir d'un certain niveau de zoom.

Différents calques regrouperont les éléments selon leurs sources: tracés OSM, corrections du mappeur et image satellite. L'utilisateur pourra désactiver certains de ces calques pour simplifier temporairement l'affichage.
Une fois qu'il aura suffisamment zoomé sur une potentielle erreur à traiter, le mappeur pourra proposer sa propre correction, c'est-à-dire modifier un polygone OSM existant, créer un polygone ou ne rien faire.
Les modifications effectuées lors d'une même session sont gardées en mémoire pour être récupérées et envoyées lors de la prochaine étape du scénario.

%\begin{itemize}
%\item Parcourir les erreurs détectées
%\item Afficher/Masquer les calques OSM
%\item Créer un nouveau bâtiment
%\item Modifier un bâtiment existant
%\item Supprimer un bâtiment
%\end{itemize}

\subsubsection{(B4) Mettre en ligne un correctif}

Après avoir préparé les correctifs à mettre en ligne, le mappeur décide d'envoyer ou non l'ensemble des correctifs effectués sur OSM. S'il ne s'est pas encore connecté à son compte OSM, cette action lui impose alors de se connecter pour être prise en compte.

Après cet envoi, l'utilisateur peut choisir de travailler sur une nouvelle zone ou de simplement quitter l'outil. 

Ce point repose principalement sur l'API d'OSM, d'une part pour la connexion du mappeur à son compte, d'autre part pour l'envoi des corrections à OSM. Il s'agit donc de potentiellement transformer et englober les polygones correctifs avec les metadonnées nécessaires à leur interprétation par OSM.
%\begin{itemize}
%\item Se connecter à son compte OSM
%\item Effectuer des corrections
%\item Envoyer les modifications à OSM
%\item Se déconnecter de son compte OSM
%\end{itemize}

\subsection{Cron : détection et stockage des erreurs}

Cette partie traite des besoins liés à l'exécution régulière du modèle pour la détermination d'erreurs sur les cartes OSM et leur sauvegarde dans une base de données.

\subsubsection{(B5) Récupérer des données satellitaires}

L'outil doit obtenir de quoi nourrir le modèle d'intelligence artificielle pour pouvoir déterminer les contours de bâtiments. Ces images satellite des villes étudiées seront fournies par le client.


%%et potentiellement sur l'ensemble du globe de vues satellitaires. 

%Le fonctionnement du modèle nécessite une période d'entraînement où celui-ci "apprend" les spécificités de la zone étudiée.% Les vues doivent donc également être nombreuses.

\subsubsection{(B6) Traiter les données (IA)}

Ensuite, l'outil envoie les données décrites ci-dessus au modèle. Déterminer le format des entrées pour le modèle et adapter les vues et polygones/contours à leurs formats respectifs est donc une autre préoccupation de ce projet.

\subsubsection{(B7) Récupérer des données OSM} 

L'outil doit récupérer les contours des bâtiments réalisés sur OSM pour pouvoir les comparer aux résultats de la boite noire dans l'étape suivante. Cela inclut la compréhension par l'outil de la zone sur laquelle récupérer les données (\textit{i.e.} la capacité d'association des vues satellitaires à une surface du globe dans OSM) et la demande aux serveurs d'OSM  des données via leur API. 

\subsubsection{(B8) Déterminer des formats I/O}

Confirmer la stabilité du format d'entrée ou fournir un moyen d'ajuster dans l'outil de manière flexible la façon de gérer les vues avant traitement s'avère également important puisque le modèle développé par le client est un travail en constante amélioration. 

Enfin, de façon similaire, déterminer le format des sorties s'avère important pour s'assurer de la maintenabilité et de la validité de l'outil.



%Le fonctionnement global de l'application web peut entièrement varier selon le temps requis pour l'exécution optimale du modèle. En effet, selon si la récupération des polygones en sortie se fait en secondes, en minutes ou en heures, la façon dont l'outil proposera au mappeur de venir corriger les potentielles erreurs changera profondément. 

\subsubsection{(B9) Détecter des erreurs }
Afin de détecter les erreurs, il est nécessaire de comparer les données OSM et les polygones obtenus grâce au modèle afin de constater des divergences. Le script au coeur de l'outil permettant cette comparaison repose sur un algorithme au fonctionnement prouvé et à l'implémentation validée via divers tests unitaires et d'intégration. 

\subsubsection{(B10) Stocker des erreurs}

Les résultats du script mentionné précédemment sont les erreurs qui seront affichées pour les mappeurs voulant corriger la cartographie OSM. Puisque l'ensemble du scénario décrit actuellement est interne et dissocié de la partie interagissant avec l'utilisateur, il est nécessaire de stocker les résultats pour leur utilisation future. L'outil effectue donc ce stockage de manière à faciliter au maximum son interprétation future: stockage des erreurs avec les tracés OSM associés, timestamps, version du modèle... 

\subsubsection{(B11) Itération du procédé } \label{B11}

L'outil met en place un scheduler répétant le processus indiqué ci-dessus de manière automatique et régulière pour traiter diverses zones selon la vieillesse des derniers traitements. 

\subsubsection{Tests}

L'ensemble des tests réalisés vérifieront la conformité de l'implémentation aux spécifications énoncés ci-dessus, pour les besoins fonctionnels indiqués ci-dessus. Ces tests fonctionnels s'appuieront sur les scénarios décrits dans la section \ref{section:scenario}.

\section{Besoins non fonctionnels}

Parmi les besoins non fonctionnels du projet, nous comptons :

\subsubsection{(BNF12) Ergonomie}

L'interface web doit être intuitive et facile d'utilisation pour les mappeurs agissant sur les tracés. En particulier les manipulations suivantes devront être réalisables sur la carte :
\begin{itemize}
    \item Zoomer / dezoomer
    \item Translater la carte
    \item Manipuler des polygones en bougeant un point (drag and drop), supprimant un point (sup) ou ajoutant un point (double clic)
    \item Afficher le calque des polygones OSM
    \item Le fond de carte présent sur notre interface correspond à celui de OSM
    \item Les erreurs sont regroupées à l'aide d'une bulle qui indique leur nombre dans une zone. Elles ne sont affichées que lorsqu'on a suffisamment zoomé sur un endroit de la carte.
\end{itemize}


\subsubsection{(BNF13) Réactivité}

L'interface doit être réactive lors de la navigation, du chargement des erreurs et du déplacement des points. C'est pour cela qu'il a été choisi de calculer les erreurs en amont et les stocker en base de données plutôt que réaliser cette action sur le moment. 

\subsubsection{(BNF14) Versioning (scheduler)}

Les données de OSM pouvant être différentes entre le moment où l'utilisateur utilise notre application et le moment où la détection d'erreur a été réalisée, un versioning des erreurs indiquant la date de détection sera réalisé. Par ailleurs la version de la boite noire sera également sauvegardée car celle-ci est susceptible d'évoluer.

Un système de scheduler cyclique sera réalisé pour mettre en application ce versioning (cf. (B11) Itération du procédé).

\subsubsection{(BNF15) Seuil de comparaison}

Le script de détection détermine une erreur en fonction d'un seuil de tolérance statique fixé de façon arbitraire. \medbreak