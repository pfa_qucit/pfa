\chapter{Conception}

\section*{Architecture}

Pour représenter la façon dont l'outil fonctionne en interne, nous nous basons sur la représentation des acteurs donnée en \ref{fig:boundary}. La décomposition des acteurs présentés et la précision de leurs composants et fonctions nous permet de comprendre la façon dont les différents aspects de l'outil se compilent et interagissent.

\section{Diagramme de Gantt} \label{section:gantt}

À partir des besoins exprimés dans la section \ref{chapter:deux}, le projet a pu être segmenté en différentes tâches sur lesquelles ont été identifiées des dépendances. Une dépendance est représentée par une flèche. Il est possible de repérer un groupe de "tâches dépendantes" par le code couleur sur les figures \ref{fig:ganttbis} et \ref{fig:gantt}.\medbreak 

Le lien entre les tâches est fait de telle sorte que plusieurs travaux soient réalisables en parallèle. Nous avons pu de fait établir un planning prévisionnel visible sur la figure \ref{fig:gantt}. Une description de chaque tâche (ou module) est disponible en dessous des figures. \medbreak

Les différents livrables qui seront fournis au cours du développement (jalons) correspondent à des prototypes fonctionnels de l'application.

\newpage
\begin{figure}[H]
\vspace*{-30mm}
\hspace*{-27mm}
\centering
\includegraphics[width=1.35\textwidth]{figures/ganttbis}
\captionof{figure}{Graphe des tâches}
\label{fig:ganttbis}
\end{figure}

\begin{figure}[H]
\vspace*{-3mm}
\hspace*{-28mm}
\centering
\includegraphics[width=1.35\textwidth]{figures/gantt}
\captionof{figure}{Diagramme de Gantt}
\label{fig:gantt}
\end{figure}


Les différentes tâches sont décrites brièvement ci-dessous : \medbreak
\begin{itemize}
    \item[Auth OSM :] utilisation de l'API d'OSM pour l'authentification d'un mappeur \textit{(B7)}
    \item[Récupération des données OSM :] utilisation de l'API d'OSM pour la récupération d'informations sur les contours, les zones, etc. \textit{(B7)}
    \item[GUI OSM sans API :] front-end du site web (fond de carte, navigation...) \textit{(B1)}
    \item[GUI + Interaction Polygone :]  manipulation d'un polygone (drag and drop) \textit{(B3)}
    \item[GUI + Polygone OSM :] affichage de polygones récupérés depuis OSM \textit{(B2)}
    \item[GUI + Prédictions :] affichage des erreurs prédites par le modèle \textit{(B2)}
    \item[Méthode IoU :] implémentation de l'algorithme de comparaison de polygones \textit{(B9)}
    \item[Récupération .tiff :] récupération des images satellites \textit{(B8)}
    \item[Traitement .tiff :] traitement sur les images satellites pour pouvoir les convertir au format d'entrée du modèle \textit{(B8)}
    \item[Interaction Modèle :] mise en relation de tâches pour faire fonctionner la boite noire \textit{(B6)}
    \item[BDD :] implémentation du modèle relationnel et préparation des requêtes / optimisations utiles \textit{(B10)}
    \item[Scheduler :] automatisation de la recherche d'erreurs \textit{(B11)}
    \item[Livrable V1] : interface graphique brute permettant la navigation sur une carte et la manipulation de polygones en utilisant des données faussaires \textit{(B1, B2, B3, B7)}
    \item[Livrable V2] : LV1 + les données utilisées proviennent d'une BDD \textit{(B2, B10)}
    \item[Livrable V3] : LV2 + possibilité de \textit{submit} les modifications apportées sur OSM \textit{(B4)}
\end{itemize}

\section{Réutilisation du projet DeepOSM}

Le projet DeepOSM \cite{deeposm} est un projet d'analyse de routes à l'aide d'un modèle d'intelligence artificielle et d'images satellites. Ce projet consiste à entrainer un modèle et comparer les prévisions fournies par celui-ci avec les données de OSM afin de détecter des erreurs eventuelles dans le tracé des routes.  \medbreak

Dans cette partie, nous allons reprendre les différents modules décrits dans la partie \ref{section:gantt} et indiquer pour chacun s'il est possible de réutiliser le code de DeepOSM pour remplir la fonctionnalité souhaitée. \bigbreak

\subsubsection{Auth OSM}

Il n'y a pas de phase d'authentification à OSM dans DeepOSM. En effet l'application suggère d'effectuer directement les modifications sur OpenStreetMap en étant redirigé vers la plateforme (cf. \ref{gui}).

\subsubsection{Récupération des données OSM} 

Les données récupérées par DeepOSM sont obtenues au format .osm.pbf à partir du site Geofabrik \cite{geofabrik}.
Le format .osm.pbf est un format permettant l'échange de données OSM brutes. Ces fichiers contiennent toutes les données et métadonnées disponibles sur OSM pour une région donnée. \medbreak

Ces données sont traitées à l'aide de la bibliothèque Python Pyosmium \cite{pyosmium} qui est une encapsulation de la bibliothèque C++ Osmium. Celle-ci permet le parsing de fichiers OSM et nous l'utiliserons donc pour la récupération des polygones OSM qui correspondent à des bâtiments. \medbreak 

En particulier, nous allons nous inspirer de la classe \textit{WayExtracter} et des méthodes contenues dans le fichier \texttt{DeepOSM/src/openstreetmap\_labels.py}.

\subsubsection{GUI OSM (et ses dérivées)} \label{gui}
DeepOSM ne possède pas d'interface graphique de la carte à proprement parler. \medbreak

La solution utilisée (fichier \texttt{DeepOSM/src/training\_visualization.py}) consiste à générer un fichier JPEG sur lequel on affiche les routes et les erreurs détectées pour une zone donnée. \medbreak

La vue de l'interface web concernant les erreurs (fichier \texttt{/website/templates/view\_error.html}) affiche cette image et permet de la valider ou de l'invalider (flag bad prediction). Une redirection vers OSM est proposée afin de corriger directement l'erreur.

\begin{lstlisting}[language=html]
<a class="btn btn-success" target="_blank"
href=https://www.openstreetmap.org/edit#map=18/{{center.0}}/{{center.1}}
>Fix in OpenStreetMap</a>
\end{lstlisting} \medbreak

Il ne sera pas possible de réutiliser du code dans l'ensemble des tâches liées à GUI OSM étant donné qu'il n'y a pas d'interface de ce genre dans DeepOSM.


\subsubsection{Méthode IoU }

L'utilisation de la méthode IoU est propre à notre projet et n'est pas présente dans le projet DeepOSM. %L'implémentation et l'application aux polygones manipulés s'inspirera cependant de la méthode évoquée dans la section scoring du challenge SpaceNet \cite{sectionscoring}. 
Nous n'allons pas pour autant l'implémenter nous même, mais utiliser un code Python déjà existant, extrait d'un autre projet, afin de comparer deux CSV contenant les polygones.


\subsubsection{Récupération .tiff}
 
La récupération des images satellites se fait en téléchargement depuis le serveur S3 de NAIP. Puisque les images utilisées seront directement disponibles sur le disque dur où se situeront l'interface et les scripts d'interaction avec le modèle, nous n'aurons pas besoin de télécharger depuis un serveur extérieur les images. 
D'autre part, il existe une bibliothèque libre, GDAL (\url{http://www.gdal.org/}), qui permet de traiter les fichiers GeoTIFF, fichiers utilisant les métadonnées du format '.tiff' pour ajouter des données utiles au format satellite. Cette bibliothèque permet donc de récupérer les données correspondant à des coordonnées GPS particulières.
Le projet DeepOSM utilise d'ailleurs cette bibliothèque, dans le fichier \texttt{src/training\_data.py} pour ouvrir des images NAIP.

\subsubsection{Traitement .tiff}

Le fichier \texttt{src/training\_data.py} de DeepOSM se charge de charger un fichier .tiff, des images bitmaps correspondant aux routes réelles, pour l'entraînement de la boite noire  :
\begin{itemize}
    \item Données .tiff chargées en mémoire (fichier naip\_images.py).
    \item Extraction des routes sous forme de segments via la fonction way\_bitmap\_for\_naip ("waymap")
\end{itemize} \medbreak



\subsubsection{Interaction Modèle}

L'interaction avec le modèle est précisée dans le fichier \texttt{src/single\_layer\_network.py} (réseau de neurone à une couche). Le format d'entrée est-celui du fichier formaté dans \textit{Traitement .tiff} et la sortie de la boite noire correspond à un tableau \textit{numpyarray}. \medbreak

Pour effectuer une prédiction sur une image, on se contente d'appeler la méthode \textit{model.predict(IMG)}. Il est supposé que le fonctionnement sera similaire avec la boite noire fournie par le client.


%http://www.machinalis.com/blog/python-for-geospatial-data-processing/
%http://www.gdal.org/frmt_gtiff.html

\subsubsection{BDD } 

Le fichier \texttt{deeposm.org/website/models.py} contient une définition d'une erreur (à savoir, la différence entre une prédiction et une donnée OSM) en base de données. Ce modèle d'erreur comprend des informations sur sa localisation géographique et sa date de création. Nous pourrons reprendre ce modèle dans notre base de données, tout en retirant les informations trop spécifiques (état des US, flag utilisé dans l'application web). 

\subsubsection{Scheduler }

Le fichier \verb|bin/update_deeposmorg.py| présente un batch de mise  à jour des données de tous les états utilisés dans le cadre du projet DeepOSM. Ce script pourra être adapté à ce projet, en revanche, du travail supplémentaire devra être effectué pour proposer une automatisation de l'appel de ce script, et pour éventuellement diviser le batch en plusieurs sous-batches réalisés à des dates différentes. 

\section{Base de données}

Le schéma de base de donnée suivant résume une idée à priori de la représentation des données utiles pour le stockage des erreurs détectées par le modèle avant correction par un mappeur. Bien qu'étant par nature sujet à optimisation et à légère modification, il représente dans les grandes lignes ce qui sera retenu sur le serveur web :
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth/2]{figures/bdd}
\captionof{figure}{Diagramme entité-association}
\label{fig:bdd}
\end{figure}

\section{Contraintes techniques}

Ce chapitre compile l'ensemble des considérations techniques à prendre en compte au cours du projet et connues à priori par le client et l'équipe. Les contraintes techniques telles que les logiciels et langages utilisés sont également abordées ici.

\subsubsection{Hébergement : }

Le site web servant comme interface pour l'utilisateur est stocké sur un serveur mis à disposition par le client. A terme, ce serveur stockera les fichiers utiles à l'affichage du site, les scripts utiles à l'interaction avec le modèle d'IA, la base de données qui intègre les erreurs détectées par le modèle évoqué. Il gérera également le lancement des scripts de façon automatique et régulière.

Il est à différencier du serveur mis en place par le client et qui contient les modèles d'IA et les capacités de calcul propres au fonctionnement de ces derniers.

\subsubsection{Programmation : }
\begin{itemize}
    \item[Front-end : ]  Afin de faciliter le développement de l'outil, l'équipe propose l'utilisation de Leaflet\cite{leaflet}, une librairie Javascript facilitant la manipulation de cartes et contenant les outils nécessaires à la création de polygones.
    \item[Back-end : ] Il est demandé que le code back-end à la fois pour le serveur web et les scripts soit écrit en Python. Le choix du framework pour le développement server-side est laissé libre, avec une préférence pour Flask \cite{flask}, micro-framework déjà utilisé chez le client.
    \item[BDD : ] Une base de données PostgreSQL\cite{pgsql} avec l'addon PostGIS\cite{postgis} est prévue pour faciliter le stockage de valeurs GPS et positions géographiques.
\end{itemize}


\subsubsection{Modèle : }

Le travail produit par le client en terme de deep learning et de recherches de contours se rapproche des travaux effectués en \cite{inspi}, notamment sur le format des entrées / sorties. C'est donc une base de travail solide pour préparer la connection des différents acteurs de l'outil. Elle prend en entrée des images et des données osm, et sort un CSV par image, 
contenant des polygones. Les images d'entrée sont celles du jeu de données Spacenet (décrit ci-après), donc au format \verb|.tiff|.

\subsubsection{Images satellites : }

Il a été décidé avec le client d'utiliser les images satellites fournies par \textit{Spacenet}. Notons que les zones couvertes par ce fournisseur ne sont pas exhaustives, c'est-à-dire que les images satellites fournies ne couvrent que des zones restreintes du globe auxquelles il faudra se limiter (Paris, Vegas, Khartoum, Shanghai).

\subsubsection{Comparaison de bâtiments}

Afin d'évaluer deux ensembles de polygones représentants les bâtiments, un package Python fourni lors du SpaceNetChallenge\cite{utilities} permet de comparer les-dits ensembles décris dans des fichiers CSV en utilisant la méthode IoU. Il sera possible d'utiliser ce package pour comparer les bâtiments extraits d'une image satellite avec les bâtiments OSM.

