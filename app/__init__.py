import os
from flask import Flask, render_template, session, jsonify, request
from flask_cli import FlaskCLI

app = Flask(__name__)
FlaskCLI(app)
app.secret_key = 'secret_key'

app.config.from_object('app.config.development')
if os.getenv('APP_CONFIG_FILE'):
    app.config.from_envvar('APP_CONFIG_FILE')

from views import auth, bdd_interact, osm_interact

# with app.app_context():
    # init_db() TODO ou faire l'initiatlisation
# from models import db

app.register_blueprint(auth.auth_page)
app.register_blueprint(osm_interact.osm_interact_page)
app.register_blueprint(bdd_interact.bdd_int)

@app.route('/')
def index():
    return render_template('index.html', connected=('oauth' in session))
