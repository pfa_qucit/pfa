function toggleConnectButton(status){
  if(status == StatusEnum.On){
    $("#disconnectButton").removeClass('hide');
    $('#connectButton').addClass('hide');
    connected = true;
    if (map.hasLayer(drawnItems)) {
      $('#refuseButton').attr('disabled', false);
      $('#sendChanges').attr('disabled', false);
    } else {
      $('#refuseButton').attr('disabled', true);
      $('#sendChanges').attr('disabled', true);
    }
  }else if(status == StatusEnum.Off){
    $("#disconnectButton").addClass('hide');
    $('#connectButton').removeClass('hide');
    $('#refuseButton').attr('disabled', true);
    $('#sendChanges').attr('disabled', true);
    connected = false;
    map.removeLayer(drawnItems);
    map.removeControl(drawControl);
    map.addLayer(markersLayer);
  }
}

function toggleOSMErrorsNavBar(){
  var disabled = true;
  if(!map.hasLayer(drawnItems)){
    disabled = false;
  }
  $('#prev').attr('disabled', disabled);
  $('#next').attr('disabled', disabled);
}

function actionButton(status) {
  switch (status) {
    case InteractionsEnum.Connected: // connected
    toggleConnectButton(StatusEnum.On);
    toggleOSMErrorsNavBar();
    break;
    case InteractionsEnum.Disconnected: // disconnected
    toggleConnectButton(StatusEnum.Off);
    toggleOSMErrorsNavBar();
    break;
    case InteractionsEnum.NoOSMErrors: // no errors
    toggleOSMErrorsNavBar();
    break;
    case InteractionsEnum.OSMErrorsFound: // can edit
    toggleOSMErrorsNavBar();
    break;
    default:
    console.log("actionButton abnormal input");
  }
}

// Envoie une notification
// icon : icone font-awesome
// status : couleur de l'arriere plan
function notify(icon, text, status) {
  $.notify({
    icon: 'fa ' + icon,
    title: text,
    message: ''
  },{
    element: 'body',
    type: status,
    placement: {
      from: "bottom",
      align: "right"
    },
    offset: { x: 10, y: 70 },
    z_index: 9999,
    width: "100px"
  });
}

// Verifie si des erreurs sont encore disponibles sinon notification
function errorsExist() {
  if (errors.length == 0) {
    actionButton(InteractionsEnum.NoOSMErrors);
    notify('fa-times', 'There are no errors for this zone', 'danger');
    return false;
  } else {
    actionButton(InteractionsEnum.OSMErrorsFound);
  }
  return true;
}

// Reset map view to default
function resetFocus() {
  map.setView([(errors[currentErrorIndex].lat_max+errors[currentErrorIndex].lat_min)/2, (errors[currentErrorIndex].lng_max+errors[currentErrorIndex].lng_min)/2], 18);
}

/**
* Recrée les marqueurs
*/
function reset_error_display(){
  if(errors.length == 0){
    actionButton(InteractionsEnum.NoOSMErrors);
    notify('fa-times', 'There are no errors for this zone', 'danger');
  }else{
    errors.forEach(function(err, i){
      var customMarker = null;
      // Une couleur = une erreur
      switch (err.error_type) {
        case 1:
        customMarker = redMarker;
        break;
        case 2:
        customMarker = blueMarker;
        break;
        case 3:
        customMarker = greenMarker;
        break;
        default:
        customMarker = redMarker;
        console.log("abnormal error type");
      }
      L.marker([(err.lat_max+err.lat_min)/2, (err.lng_max+err.lng_min)/2], {id: parseInt(err.item_id), type: err.type, error: err.error_type, icon: customMarker, id_database: err.id}).addTo(markersLayer).bindPopup("Loading ...");
    });
    actionButton(InteractionsEnum.OSMErrorsFound);
    resetFocus();
  }
}

// Deplace le curseur de "inc", le curseur correspond a l'erreur actuellement traitée
function nextError(inc) {
  if (!errorsExist()) {
    return;
  }
  currentErrorIndex = (currentErrorIndex + inc) % errors.length;
  if(currentErrorIndex < 0) currentErrorIndex = errors.length - 1;
  currentErrorIndex = currentErrorIndex % errors.length;
  resetFocus();
}

// Permet de rendre un polygone editable
function addEditionPoly(poly) {
  poly.enableEdit();
  poly.dragging.disable();
  poly.on('click', function (e) {
    this.editor.newHole(e.latlng); // Creation d'un trou au premier clique dans un polygon
  });
  poly.on('editable:drawing:move', function(e) {
    e.layer.options.edited = true; // Marque le polygon comme modifie
  })
}

// Supprime une erreur
// status == true => supprime de la base
function removeError(status) {
  if (status) {
    $.ajax({
      type: "DELETE",
      url: 'error/' + errors[currentErrorIndex].id,
      error: function() {
      },
      success: function() {
      }
    });
  }
  currentMarker.setOpacity(0);
  errors.splice(currentErrorIndex, 1);
  nextError(0);
  $('#sendChanges').attr('disabled', true);
  $('#refuseButton').attr('disabled', true);
  map.removeLayer(drawnItems);
  map.removeControl(drawControl);
  map.addLayer(markersLayer);
}

function sendChanges(e) {
  if (!confirm('Are you sure you want to send these changes ?')) {
    return;
  }
  $('#sendChanges').attr('disabled', true);
  $('#refuseButton').attr('disabled', true);
  var layers = drawnItems._layers;
  var ways = [];
  var currentIndexData = 0;
  for (var i in layers) {
    if (layers[i].options.edited == undefined || layers[i].options.edited == true) {
      if (layers[i].options.edited == true && layers[i].options.id != undefined) {
        deletedPolygons.push({id: layers[i].options.id, type: layers[i].options.type});
      }
      ways.push({'outer': [], 'inner': []});
      var points = layers[i]._latlngs;
      ways[currentIndexData]['outer'] = points[0];
      for (var j = 1; j < points.length; j++) {
        if (Array.isArray(points[j][0])) {
          for (var k = 0; k < points[j].length; k++) {
            if (points[j][k].length > 0) {
              ways[currentIndexData]['inner'].push(points[j][k]);
            }
          }
        } else {
          if (points[j].length > 0) {
            ways[currentIndexData]['inner'].push(points[j]);
          }
        }
      }
      currentIndexData++;
    }
    drawnItems.removeLayer(i);
  }
  $.ajax({
    type: "POST",
    url: 'osm/' + errors[currentErrorIndex].id,
    data: JSON.stringify({ways: ways, toDelete: deletedPolygons}),
    contentType: "application/json; charset=utf-8",
    error: function() {
    },
    success: function() {
    }
  });
  removeError(false);
}
