var map = L.map('llmap', {editable: true}).setView([0, 0], 2);
var bing = new L.BingLayer("Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol", {
  type: "Aerial",
  maxZoom: 21,
  maxNativeZoom: 19 // maxNativeZoom bloque le changement de taille de résolution supérieur
  //    et zoom sur l'image jusqu'à maxZoom
});  // AuhiCJHlGzhg93IqUH_oCpl_-ZUrIE6SPftlyGYUvr9Amx5nzA-WqGcPquyFZl4L
var OpenStreetMap_France = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
  maxZoom: 21,
  maxNativeZoom: 19,
  opacity: 0
});

map.addLayer(bing);
map.addLayer(OpenStreetMap_France);

var selectedZoneId = -1;
var connectPopUp;
var connectionInterval;

var errors = [];
var currentErrorIndex = 0;
var currentMarker = null;

var deletedPolygons = [];

var InteractionsEnum = Object.freeze({ Connected: 1, Disconnected: 2, NoOSMErrors: 3, OSMErrorsFound: 4  });
var StatusEnum = Object.freeze({On: 1, Off: 2});

var drawnItems = new L.FeatureGroup();
var markersLayer = L.featureGroup().addTo(map);
var drawControl = new L.Control.Draw({
  draw: {
    polyline: false,
    rectangle: false,
    circle: false,
    marker: false,
    circlemarker: false,
    polygon: {
      icon: new L.DivIcon({
        iconSize: new L.Point(10, 10),
        className: 'leaflet-div-icon leaflet-editing-icon my-own-class'
      }),
      tooltip: null
    }
  },
  edit: {
    featureGroup: drawnItems,
    edit: false
  }
});

var redMarker = L.divIcon({
  className: "no",
  iconAnchor: [0, 24],
  labelAnchor: [-6, 0],
  popupAnchor: [0, -36],
  html: `<span class="marker shift-marker red-marker" />`
});

var blueMarker = L.divIcon({
  className: "no",
  iconAnchor: [0, 24],
  labelAnchor: [-6, 0],
  popupAnchor: [0, -36],
  html: `<span class="marker shift-marker blue-marker" />`
});

var greenMarker = L.divIcon({
  className: "no",
  iconAnchor: [0, 24],
  labelAnchor: [-6, 0],
  popupAnchor: [0, -36],
  html: `<span class="marker shift-marker green-marker" />`
});

$('#modalZone').on('hide.bs.modal', function (e) {
  if (selectedZoneId == -1) {
    e.preventDefault();
  }
});

$('#modalZone').modal('show');

$('#changeZoneMenu').on('click', function (e) {
  $('#modalZone').modal('show');
});

$('#infoMenu').on('click', function (e) {
  $('#modalInfo').modal('show');
})

$('#zoneSelectConfirm').on('click', function (e) {
  selectedZoneName = $("#zoneSelectId").text().trim();
  $.get("zone_errors/"+selectedZoneName, function(data){
    errors = data.errors;
    currentErrorIndex = 0;
    reset_error_display();
  });
  if (selectedZoneId == -1) {
    $('#modalZoneTitle').text('Change zone');
    $('#zoneSelectClose').removeClass('hide');
  }
  selectedZoneId = $('#zoneSelectId').val();  // missing doc on why its here
  $('#modalZone').modal('hide');
  map.removeLayer(drawnItems);
  map.removeControl(drawControl);
  map.addLayer(markersLayer);
  $('#refuseButton').attr('disabled', true);
  $('#sendChanges').attr('disabled', true);
});

$('#acceptChange').on('click', function(e){
  console.log("Change OK");
});

/*
* AUTHENTIFICATION
*
*/
$('#disconnectButton').on('click', function(e){
  $.ajax({
    url : 'disconnect',
    type : 'GET'
  }).done(function( data ) {
    actionButton(InteractionsEnum.Disconnected);
    toggleOSMErrorsNavBar();
    notify('fa-check', 'Successful disconnection', 'success');
  });
});

$('#connectButton').on('click', function(e) {
  var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
  var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
  var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
  var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
  var left = ((width / 2) - 275) + dualScreenLeft;
  var top = ((height / 2) - 375) + dualScreenTop;

  localStorage.setItem('isConnected', 0);
  connectPopUp = window.open("/connect", "popupWindow", "width=" + 550 + ",height=" + 750 + ",scrollbars=yes" + ", top=" + top + ", left=" + left);
  connectionInterval = setInterval(function()  {
    if (localStorage.getItem('isConnected') == 1) {
      localStorage.setItem('isConnected', 0);
      actionButton(InteractionsEnum.Connected);
      connectPopUp.close();
      clearInterval(connectionInterval);
      notify('fa-check', 'Successful connection', 'success');
    }
  }, 1000);
});

/** NAVIGATION  **/

$('#prev').on('click', function(e) {
  nextError(-1);
});

$('#next').on('click', function(e) {
  nextError(1);
});

$('#sendChanges').on('click', sendChanges);

$('#refuseButton').on('click', function(e){
  if (confirm('Are you sure this building is exact ?')) {
    removeError(true);
  }
});

$('#prev').tooltip({});
$('#next').tooltip({});
$('#sendChanges').tooltip({});
$('#refuseButton').tooltip({});

/*
* DRAW INTERACTION
*
*/

map.on(L.Draw.Event.CREATED, function (e) {
  var type = e.layerType,
  layer = e.layer;
  drawnItems.addLayer(layer);
  addEditionPoly(layer);
});

map.on('draw:deleted', function (e) {
  for (var i in e.layers._layers) {
    var layer = e.layers._layers[i];
    if (layer.options.id != undefined) {
      deletedPolygons.push({id: parseInt(layer.options.id), type: layer.options.type});
    }
  }
});

map.on('editable:vertex:deleted', function (e) {
  e.layer.options.edited = true;
});

markersLayer.on("click", function(e) {
  if (connected) {
    currentErrorIndex = errors.findIndex(x => x.id == e.layer.options.id_database);
    currentMarker = e.layer;

    switch (e.layer.options.error) {
      case 1: // NO_MATCH - pas de polygone OSM
      notify("", "This building doesn't match with a polygon OSM.", "info");
      break;
      case 2: // NO_PREDICTION - pas de prédiction pour un polygon
      notify("", "This polygon OSM doesn't match with a real building.", "info");
      break;
      case 3: // Polygone OSM pas exact
      notify("", "This building is not well represented on OSM.", "info");
      break;
      default:
      console.log("abnormal error type");
    }
    deletedPolygons = [];

    if (e.layer.options.type != 'multipolygon' && e.layer.options.error != 1) {
      $.ajax({
        url: "building/"+ e.layer.options.id,
        method: "GET",
        data: {type: e.layer.options.type}
      }).done(function( data ) {
        var latlngs = [
          [], // outer ring
          [[]] // hole : bug on ne peut pas supprimer le premier trou donc on crée un premier noeud vide
        ];
        outerPath = data.outer[0];
        for (var i = 0; i < outerPath.order.length ; i++) {
          order = outerPath.order[i];
          latlngs[0].push(outerPath.nodes[order]);
        }

        for (i = 0; i < data.inner.length ; i++) {
          latlngs[1].push([]);
          innerPath = data.inner[i];
          for (var j = 0; j < innerPath.order.length ; j++) {
            order = innerPath.order[j];
            latlngs[1][i+1].push(innerPath.nodes[order]);
          }
        }

        map.removeLayer(markersLayer);
        map.addLayer(drawnItems);
        map.addControl(drawControl);
        addEditionPoly(L.polygon(latlngs, {id : e.layer.options.id, type: e.layer.options.type, edited: false}).addTo(drawnItems));
        $('#sendChanges').attr('disabled', false);
        $('#refuseButton').attr('disabled', false);
        toggleOSMErrorsNavBar();
      }).fail(function(data) {
        notify('fa-times', 'This error has already been corrected.', 'danger');
        map.closePopup();
        currentMarker.setOpacity(0);
        errors.splice(currentErrorIndex, 1);
        nextError(0);
      });
    } else if (e.layer.options.type == 'multipolygon') {
      $.ajax({
        url: "osm/"+ e.layer.options.id_database,
        method: "GET"
      }).done(function( data ) {
        var nodes = data['nodes'];
        var polygons = data['polygons'];

        $('#sendChanges').attr('disabled', false);
        $('#refuseButton').attr('disabled', false);
        toggleOSMErrorsNavBar();

        map.removeLayer(markersLayer);
        map.addLayer(drawnItems);
        map.addControl(drawControl);

        for (var i = 0; i < polygons.length; i++) {
          var latlngs = [ [], [[]] ];
          outerPath = polygons[i].outer;
          for (var j = 0; j < outerPath.length ; j++) {
            order = outerPath[j];
            latlngs[0].push(nodes[order]);
          }

          for (j = 0; j < polygons[i].inner.length ; j++) {
            latlngs[1].push([]);
            innerPath = polygons[i].inner[j];
            for (var k = 0; k < innerPath.length ; k++) {
              order = innerPath[k];
              latlngs[1][j+1].push(nodes[order]);
            }
          }

          if (polygons.length > 5) {
            var poly = L.polygon(latlngs, {id : polygons[i]['id'], type: polygons[i]['type'], edited: undefined}).addTo(drawnItems);
            poly.on('click', function (e) {
              if (e.target.options.edited == undefined) {
                addEditionPoly(this);
                e.target.options.edited = false;
              }
            });
          } else {
            addEditionPoly(L.polygon(latlngs, {id : polygons[i]['id'], type: polygons[i]['type'], edited: false}).addTo(drawnItems));
          }
        }
      }).fail(function(data) {
        notify('fa-times', 'This error has already been corrected.', 'danger');
        map.closePopup();
        currentMarker.setOpacity(0);
        errors.splice(currentErrorIndex, 1);
        nextError(0);
      });
    } else if (e.layer.options.error == 1) {
      map.removeLayer(markersLayer);
      map.addLayer(drawnItems);
      map.addControl(drawControl);
      $('#sendChanges').attr('disabled', false);
      $('#refuseButton').attr('disabled', false);
      toggleOSMErrorsNavBar();
    }
  } else {
    notify("fa-times", "You have to be connected", "danger");
    map.closePopup();
  }
});

L.DomEvent.addListener(document, 'keyup', function (e) {
  if (e.code == 'KeyX') {
    for (var i in drawnItems._layers) {
      drawnItems._layers[i].setStyle({fillOpacity: 0.1});
    }
  } else if (e.code == 'KeyC') {
    OpenStreetMap_France.setOpacity(0);
  }
}, map);

L.DomEvent.addListener(document, 'keydown', function (e) {
  if (e.code == 'KeyX') {
    for (var i in drawnItems._layers) {
      drawnItems._layers[i].setStyle({fillOpacity: 0});
    }
  } else if (e.code == 'KeyC') {
    OpenStreetMap_France.setOpacity(0.95);
  }
}, map);
