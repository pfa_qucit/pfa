from sqlalchemy import MetaData, Column, Integer, String, Date, ForeignKey, Float
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
import logging as log
from enum import Enum

metadata = MetaData()

Base = declarative_base(metadata=metadata)


class Zone(Base):
    __tablename__ = "zone"
    id = Column(Integer, primary_key=True)
    name = Column(String(140), nullable=False)
    areas = relationship('Area', back_populates='zone')

    def __init__(self, name):
        self.name = name

class Area(Base):
    __tablename__ = "area"
    id = Column(Integer, primary_key=True)
    file_name = Column(String(140), nullable=True)
    timestamp = Column(Date, nullable=True)
    AI_version = Column(Integer, nullable=True)
    zone_id = Column(Integer, ForeignKey('zone.id'), nullable=True)
    zone = relationship("Zone", back_populates="areas")
    errors = relationship('Error', back_populates='area')

    def __init__(self, file_name, timestamp, AI_version, zone):
        self.file_name = file_name
        self.timestamp = timestamp
        self.AI_version = AI_version
        self.zone_id = zone.id
        self.zone = zone

class ErrorType(Enum):
    NO_MATCH = 1
    NO_PREDICTION = 2
    LOW_IOU = 3

class Error(Base):
    __tablename__ = "error"
    id = Column(Integer, primary_key=True)
    lat_min = Column(Float(), nullable=True)
    lng_min = Column(Float(), nullable=True)
    lat_max = Column(Float(), nullable=True)
    lng_max = Column(Float(), nullable=True)
    polygon_id = Column(String(), nullable=True)
    item_type = Column(String(), nullable=True)#None,relation or way
    iou_level = Column(Float(), nullable=True)#1 for NO_MATCH,2 for NO_PREDICTION,3 for LOW_IOU
    error_type = Column(Integer(), nullable=True)
    area_id = Column(Integer(), ForeignKey('area.id'), nullable=True)
    area = relationship("Area", back_populates="errors")

    def __init__(self, lat_min, lng_min, lat_max, lng_max, polygon_id, item_type,error_type,iou_level):
        self.lat_min = lat_min
        self.lng_min = lng_min
        self.lat_max = lat_max
        self.lng_max = lng_max
        self.polygon_id = polygon_id
        self.item_type = item_type
        self.error_type = error_type
        self.iou_level = iou_level

    def setArea(self, a):
        self.area = a
        self.area_id = a.id

    def display(self):
        print("polygon id : ",self.polygon_id)
        print("bounds : ",self.lat_min,self.lat_max,self.lng_min,self.lng_max)
        print("error type : ",self.error_type)
        print("iou level : ",self.iou_level)
