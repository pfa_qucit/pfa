from flask import Blueprint, redirect, jsonify
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from metadb import Error, Zone, Area
from app import app

bdd_int = Blueprint('bdd_int', __name__, template_folder='../templates')

# Recupere l'ensemble des erreurs d'une zone
@bdd_int.route('/zone_errors/<string:zonename>')
def zone_errors(zonename):
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    session = Session(engine)
    errors = session\
        .query(Error)\
        .join(Area, Error.area_id == Area.id)\
        .join(Zone, Area.zone_id == Zone.id)\
        .filter(Zone.name == zonename).all()
    ret = []
    for error in errors:
        ret.append({'id': error.id, 'item_id': error.polygon_id, 'lat_min': error.lat_min, 'lat_max': error.lat_max, 'lng_min': error.lng_min, 'lng_max': error.lng_max, 'type': error.item_type, 'error_type': error.error_type})
    return jsonify({'errors': ret})

# Supprime une erreur de la base
@bdd_int.route('/error/<int:error_id>', methods=['DELETE'])
def delete_error(error_id):
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    session = Session(engine)
    error = session\
    .query(Error)\
    .filter(Error.id == error_id).first()
    session.delete(error)
    session.commit()
    # Traitement si on souhaite stocker une fausse erreur : csv, bdd, ...
    # get_map(error) dans osm_interact => recupere tous les batiments dans la zone de l'erreur
    # building(id) + data: { type: 'way' ou 'relation'} => recupere le batiment OSM
    return jsonify({'removed': True})
