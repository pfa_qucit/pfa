from flask import Blueprint, session, redirect, request, jsonify, render_template
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from metadb import Error
from app import app
from .. import OsmApi as o

osm_interact_page = Blueprint('osm_interact', __name__, template_folder='../templates')

# Recupere le batiment OSM
# id : identifiant OSM
# type (GET params) : way ou relation selon le type OSM
@osm_interact_page.route('/building/<int:id>')
def building(id):
    type_building = request.args.get('type')
    if (not type_building):
        return "Invalid 'type' value", 500
    if type_building == 'relation':
        OSM = o.OsmApi(config=app.config)
        ways = OSM.RelationGet(id)['member']
        ret = {'outer': [], 'inner': []}
        for i in range(len(ways)):
            way_id = ways[i]['ref']
            ret[ways[i]['role']].append(draw_order_node_craft(OSM, way_id))
        return jsonify(ret)
    elif type_building == 'way':
        OSM = o.OsmApi(config=app.config)
        ret = {'outer': [], 'inner': []}
        ret['outer'].append(draw_order_node_craft(OSM, id))
        return jsonify(ret)

# INNER methods
def draw_order_node_craft(api, way_id):
    nodesCoords = {}
    all_info = api.WayFull(way_id)          # WayFull ne donne pas les chemins dans l'ordre
    node_order = api.WayGet(way_id)['nd']   # WayGet retourne uniquement les ID des noeuds dans l'ordre
    for j in range(len(all_info)):
        if all_info[j]['type'] == 'node':
            curr_node = all_info[j]['data']
            nodesCoords[curr_node['id']] = {'lat': curr_node['lat'], 'lon': curr_node['lon']}
    return {'id': way_id, 'nodes': nodesCoords, 'order': node_order}

# Genere un chemin et l'envoie a OSM
# NODES : tableau de noeuds
# role : inner ou outer
# inRelation : si false ajoute le tag building au chemin
def generate_way(OSM, NODES, role, inRelation):
    if (len(NODES) > 0):
        nodes = []
        for i in range(len(NODES)):
            nodes.append(OSM.NodeCreate({ 'lat': NODES[i]['lat'], 'lon': NODES[i]['lng'], 'tag': {}})['id'])
        nodes.append(nodes[0])
        if not inRelation:
            tags = {'building': 'yes'}
        else:
            tags = {}
        way = OSM.WayCreate({ 'nd': nodes, 'tag': tags })
    return { 'ref': way['id'], 'role': role, 'type': 'way' }

# Genere une relation
# data : {'outer': [], 'inner': [[]]}
def generate_relation(OSM, data):
    ways = []
    ways.append(generate_way(OSM, data['outer'], 'outer', True))
    inners = data['inner']
    for i in range(len(inners)):
        ways.append(generate_way(OSM, inners[i], 'inner', True))
    return OSM.RelationCreate({'member': ways , 'tag': {'building': 'yes', 'type': 'multipolygon'}})

# Supprime un chemin et ses noeuds sur OSM
def delete_way(OSM, way_id):
    try:
        way = OSM.WayGet(way_id)
        OSM.WayDelete(way)
        for i in range(len(way['nd'])-1):
            try:
                OSM.NodeDelete(OSM.NodeGet(way['nd'][i]))
            except:
                print "Node " + str(way['nd'][i]) + " was already deleted"
    except:
        print "Way " + str(way_id) + " was already deleted"

# Supprime une erreur
# Cree une nouvelle relation ou un nouveau chemin dans un changeset
# Necessite une connexion OSM
@osm_interact_page.route('/osm/<int:error_id>', methods=['POST'])
def edit_building(error_id):
    if ('oauth' in session):
        data = request.get_json()
        if (data == None):
            return "Invalid value", 500
        # Verifie si l'erreur existe toujours en base
        engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
        dbSession = Session(engine)
        error = dbSession.query(Error).filter(Error.id == error_id).first()
        if error == None:
            return "Invalid error", 500
        OSM = o.OsmApi(oauth=session['oauth'], config=app.config)
        changeset = OSM.ChangesetCreate()
        for i in range(len(data['toDelete'])):
            if data['toDelete'][i]['type'] == 'way':
                delete_way(OSM, data['toDelete'][i]['id'])
            elif data['toDelete'][i]['type'] == 'relation':
                try:
                    relation = OSM.RelationGet(data['toDelete'][i]['id'])
                    OSM.RelationDelete(relation)
                    for i in range(len(relation['member'])):
                        delete_way(OSM, relation['member'][i]['ref'])
                except:
                    print "Relation " + str(data['toDelete'][i]['id']) + " was already deleted"
        for i in range(len(data['ways'])):
            if len(data['ways'][i]['inner']) > 0:
                generate_relation(OSM, data['ways'][i])
            else:
                generate_way(OSM, data['ways'][i]['outer'], 'outer', False)
        OSM.ChangesetUpdate()
        OSM.ChangesetClose()
        dbSession.delete(error)
        dbSession.commit()
        return "ok"
    else:
        return "Authentification failed", 401

# Retourne l'ensemble des batiments contenus dans la zone de l'erreur
# Retourne l'ensemble des noeuds dans la zone et pour chaque batiment une liste de noeuds ordonnes
@osm_interact_page.route('/osm/<int:error_id>', methods=['GET'])
def get_map(error_id):
    # Verifie si l'erreur existe toujours en base
    engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
    dbSession = Session(engine)
    error = dbSession.query(Error).filter(Error.id == error_id).first()
    if error == None or error.item_type != 'multipolygon':
        return "Invalid error", 500
    OSM = o.OsmApi(config=app.config)
    map = OSM.Map(error.lng_min, error.lat_min, error.lng_max, error.lat_max)
    relations = []
    ways = []
    waysRelation = []
    nodes = {}
    # trier les donnees recuperees
    for i in range(len(map)):
        if map[i]['type'] == 'node':
            nodes[map[i]['data']['id']] = {'lat': map[i]['data']['lat'], 'lon': map[i]['data']['lon']}
        elif map[i]['type'] == 'way' and map[i]['data']['tag'].has_key('building'):
            ways.append(map[i]['data'])
        elif map[i]['type'] == 'way':
            waysRelation.append(map[i]['data'])
        elif map[i]['type'] == 'relation' and map[i]['data']['tag'].has_key('building'):
            relations.append(map[i]['data'])

    # nodes : totalite des noeuds de la zone
    # polygons : liste de liste ordonnee de noeuds (chaque element est un batiment)
    data = {'polygons': [], 'nodes': nodes}
    for i in range(len(relations)): # pour chaque relation
        poly = {'id': relations[i]['id'], 'type': 'relation', 'outer': [], 'inner': []}
        for j in range(len(relations[i]['member'])): # chaque chemin de la relation
            for k in range(len(waysRelation)): # on cherche ses informations
                if waysRelation[k]['id'] == relations[i]['member'][j]['ref']:
                    if relations[i]['member'][j]['role'] == 'outer':
                        poly['outer'] = waysRelation[k]['nd']
                        break
                    elif relations[i]['member'][j]['role'] == 'inner':
                        poly['inner'].append(waysRelation[k]['nd'])
                        break
        data['polygons'].append(poly)
    for i in range(len(ways)): # pour chaque chemin (batiment sans trou)
        data['polygons'].append({'id': ways[i]['id'], 'type': 'way', 'outer': ways[i]['nd'], 'inner': []})

    return jsonify(data)
