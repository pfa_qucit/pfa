from flask import Blueprint, session, redirect, url_for, request, abort, render_template
from requests_oauthlib import OAuth1Session

auth_page = Blueprint('auth_page', __name__, template_folder='../templates')

# Redirige vers OSM pour l'authentification
@auth_page.route('/connect')
def connect():
    from app import app
    if 'oauth' in session:
        return abort(400, "Already connected")
    oauth = OAuth1Session(app.config['CONSUMER_KEY'],
                          app.config['CONSUMER_SECRET'],
                          callback_uri=app.config['CALLBACK_URL_OAUTH'])
    session['ressource_owner'] = oauth.fetch_request_token(app.config['REQUEST_TOKEN_URL'])
    return redirect(oauth.authorization_url(app.config['AUTHORIZATION_URL']))

# Adresse de retour apres l'authentifcation OSM
@auth_page.route('/oauth_return')
def oauth_authorized():
    from app import app
    if 'ressource_owner' in session:
        oauth = OAuth1Session(app.config['CONSUMER_KEY'], app.config['CONSUMER_SECRET'])
        oauth_response = oauth.parse_authorization_response(request.url)
        verifier = oauth_response.get('oauth_verifier')
        ressource_owner = session['ressource_owner']
        oauth_session = OAuth1Session(app.config['CONSUMER_KEY'],
                                      client_secret=app.config['CONSUMER_SECRET'],
                                      resource_owner_key=ressource_owner['oauth_token'],
                                      resource_owner_secret=ressource_owner['oauth_token_secret'],
                                      verifier=verifier)
        session.pop('ressource_owner')
        session['oauth'] = oauth_session.fetch_access_token(app.config['ACCESS_TOKEN_URL'])
        return render_template('connected.html', connected=('oauth' in session))
    else:
        if 'oauth' in session:
            return redirect(url_for('index'))
        return redirect(url_for('auth_page.connect'))

# Supprime la session de l'utilisateur
@auth_page.route('/disconnect')
def disconnect():
    if 'oauth' in session:
        session.pop('oauth')
    return "", 204  # valid request
